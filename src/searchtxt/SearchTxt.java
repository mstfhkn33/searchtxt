/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package searchtxt;

import Finder.FileFinder;
import GUI.MainBoard;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author mustafa.hakan.caki
 */
public class SearchTxt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*File file = new File("allComm455over33_write.txt");
        try {
            countWord(file);
        } catch (FileNotFoundException ex) {
            System.err.println("File is not found! : "+ex.toString());
        } catch (IOException ex) {
            System.err.println("File is not found! : "+ex.toString());
        }*/
        MainBoard mainBoard = new MainBoard();
        mainBoard.setVisible(true);
    }
    
    public static int countWord(File file) throws FileNotFoundException, IOException {
        int lineCounter = 0;
        BufferedReader in = new BufferedReader(new FileReader("allComm455over33_write.txt"));
        String line;
        String word01 = " E0 00 00 01";
        String word02 = " E0 00 00 02";
        String word03 = " E0 00 00 03";
        while((line = in.readLine()) != null)
        {
            if (line.length() != 0 && line.charAt(0) != '0' &&
                    (line.toLowerCase().contains(word01.toLowerCase()) == false))
            {
                System.out.println("line"+lineCounter+": "+line);
            }
            /*if (line.toLowerCase().contains(word02.toLowerCase()))
            {
                System.out.println("line"+lineCounter+": "+line);
            }
            if (line.toLowerCase().contains(word03.toLowerCase()))
            {
                System.out.println("line"+lineCounter+": "+line);
            }*/
            lineCounter++;
        }
        in.close();
        return lineCounter;
    }
    
    public static void listFiles(String folder){
        File directory = new File(folder);
        File[] contents = directory.listFiles();
        for ( File f : contents) {
        System.out.println(f.getAbsolutePath());
        }
    }
}
