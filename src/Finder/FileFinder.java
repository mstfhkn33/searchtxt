package Finder;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mustafa.hakan.caki
 */
public class FileFinder {
    
    public FileFinder(){
        foundLines = new ArrayList<>();
        totalFileNumber = 0;
    }
    
    public ArrayList<String> getFoundLines(){
        return foundLines;
    }
    
    public int getTotalFileNumber(){
        return totalFileNumber;
    }
    
    /**
     * Finds the absolute file paths in the given folder path
     * @param dirPath Keeps the folder path
     * @return all the absolute file paths in the given folder path
     */
    public int find(String dirPath){
        int lineCounter = 0;
        File file = new File(dirPath);
        ArrayList<String> lines = new ArrayList<>();
        
        foundLines.clear();
        if (file.isDirectory())
        {
            ArrayList<String> absoluteFilePaths = findAbsoluteFilePaths(dirPath);
            totalFileNumber = absoluteFilePaths.size();

            for (String absoluteFilePath : absoluteFilePaths) {
                lineCounter += find(absoluteFilePath, lines);
                foundLines.addAll(lines);
            }
        }
        else
        {
            if (file.isFile())
            {
                lineCounter += find(dirPath, foundLines);
            }
        }
        
        return lineCounter;
    }
    
    /**
     * Find lines in given file with given condition
     * @param filePath Keeps the file path
     * @param lines Keeps the found lines
     * @return number of found line
     */
    private int find(String filePath, ArrayList<String> lines){
        int lineCounter = 0;
        String line;
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filePath));
            lines.clear();
            lines.add("File : "+filePath);
            while((line = br.readLine()) != null)
            {
                if (controlStatement(line)){
                    lines.add("    "+(lineCounter+1)+". line : "+line);
                }
                lineCounter++;
            }
            return lineCounter;
        } catch (FileNotFoundException fileEx) {
            System.err.println("File is not found! : "+fileEx.toString());
            return 0;
        } catch (IOException ioEx) {
            System.err.println("File is not found! : "+ioEx.toString());
            return 0;
        }
    }
    
    /**
     * Finds the absolute file paths
     * @param folder Keeps the folder path
     * @return all found file paths
     */
    public ArrayList<String> findAbsoluteFilePaths(String folder){
        ArrayList<String> absoluteFilePaths = new ArrayList<>();
        findAbsoluteFilePathsHelper(folder, absoluteFilePaths);
        return absoluteFilePaths;
    }
    
    /**
     * Helper function for findAbsoluteFilePaths
     * @param folder Keeps the folder path
     * @param absoluteFilePaths Keeps the all found file paths
     */
    private void findAbsoluteFilePathsHelper(String folder, ArrayList<String> absoluteFilePaths){
        File directory = new File(folder);
        File[] contents = directory.listFiles();
        for ( File f : contents) {
            if (f.isFile())
            {
                absoluteFilePaths.add(f.getAbsolutePath());
            }
            else
            {
                if (f.isDirectory() && !f.isHidden())
                {
                    findAbsoluteFilePathsHelper(f.getAbsolutePath(), absoluteFilePaths);
                }
            }
        }
    }
    
    private boolean controlStatement(String line){
        String word = "Configuring incomplete,";
        if (line.toLowerCase().contains(word.toLowerCase())){
            return true;
        }
        return false;
    }
    
    private ArrayList<String> foundLines;
    private int totalFileNumber;
}
